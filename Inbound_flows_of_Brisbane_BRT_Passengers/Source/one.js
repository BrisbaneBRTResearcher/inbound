﻿/*global require*/
// require in the complete Cesium object and reassign it globally.
// This is meant for use with the Almond loader.
var mouseAtTheMap="A";
require([
        'Cesium'
    ], function(
        Cesium) {
    'use strict';
    /*global self*/
    var scope = typeof window !== 'undefined' ? window : typeof self !== 'undefined' ? self : {};

    scope.Cesium = Cesium;
	var viewer = new Cesium.Viewer( 'cesiumContainerOne', {  
		scene3DOnly: true,
		infoBox:false,
    imageryProvider : new  Cesium.ArcGisMapServerImageryProvider( {  
        url : 'http://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer'  
	} ), 
	fullscreenButton: false, 
    baseLayerPicker : false  
} );
   var labelBall = viewer.imageryLayers.addImageryProvider(new  Cesium.ArcGisMapServerImageryProvider( {  
	 url : 'http://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer'   //'http://10.211.55.5:6080/arcgis/rest/services/23386/MapServer'//
} ));  

labelBall.show = false;
var terrainProvider = new Cesium.CesiumTerrainProvider( {  
    url : '//assets.agi.com/stk-terrain/world',  
    requestVertexNormals : true  
} );  
viewer.terrainProvider = terrainProvider;  
viewer.scene.globe.enableLighting = false;  

	var scene = viewer.scene;

	// 17036014.2708 / (2 * Math.PI * 6378137.0) * 360,-3149543.7658 / (2 * Math.PI * 6378137.0) * 360


	var modelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.035392,-27.365866, 100.0));  
	var modelMatrix1 = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.023392,-27.478866, 10.0));//(153.444760,-28.560750, 10.0));  
	var modelMatrix2 = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.047498,-27.508426, 10.0));//(153.444760,-28.560750, 10.0));  

	var modelMatrix3 = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.030408,-27.50058, 10.0));//(153.444760,-28.560750, 10.0));  
	var modelMatrix4 = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.041708,-27.51388, 10.0));//(153.444760,-28.560750, 10.0));  
var model = scene.primitives.add(Cesium.Model.fromGltf({  
    url : 'Inopen_Group_Set0.gltf',//'443InOpen.gltf',//'77.gltf',//如果为bgltf则为.bgltf  
	// color : new Cesium.Color(0/255, 255/255, 0/255, parseFloat(1.0)),
	modelMatrix : modelMatrix,  
	minimumPixelSize:35,
	maximumScale:35,
	 scale : 35,
	//  attributes:{
	// 	color: Cesium.ColorGeometryInstanceAttribute.fromColor(Cesium.Color.GREEN.withAlpha(1.0))
	// }
	
}));  

// var model1 = scene.primitives.add(Cesium.Model.fromGltf({  
//     url : '1728InOpen.gltf',//如果为bgltf则为.bgltf  
// 	modelMatrix : modelMatrix1,  
	
//      scale : 35
// }));
// var model3 = scene.primitives.add(Cesium.Model.fromGltf({  
//     url : '4321InOpen.gltf',//如果为bgltf则为.bgltf  
// 	modelMatrix : modelMatrix3,  
//      scale : 35
// }));

// var model2 = scene.primitives.add(Cesium.Model.fromGltf({  
//     url : '10518InOpen.gltf',//如果为bgltf则为.bgltf  
// 	modelMatrix : modelMatrix2,  
	
//      scale : 35
// }));

// var model4 = scene.primitives.add(Cesium.Model.fromGltf({  
//     url : '23386InOpen.gltf',//如果为bgltf则为.bgltf  
// 	modelMatrix : modelMatrix4,  
//      scale : 35
// })); 

viewer.camera.flyTo({  
    destination : Cesium.Cartesian3.fromDegrees(153.02283,-27.50,28000.0)  
}); 


	function computeCircle(radius) {
    var positions = [];  
    for (var i = 0; i < 6; i++) { 
		var j = i*60;
        var radians = Cesium.Math.toRadians(j);  
        positions.push(new Cesium.Cartesian2(  
            radius * Math.cos(radians), radius * Math.sin(radians)));  
    }  
    return positions;  
	} 

var canvas = scene.canvas; 
  var handler = new Cesium.ScreenSpaceEventHandler(scene.canvas); handler.setInputAction(function (movement) { 
 var cartesian = scene.camera.pickEllipsoid(movement.endPosition, ellipsoid); 
 
 var ellipsoid = scene.globe.ellipsoid; 
 if (cartesian) { //能获取，显示坐标
 var cartographic = ellipsoid.cartesianToCartographic(cartesian);

	}
}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

viewer.camera.moveEnd.addEventListener(function(e){
	// var camera = viewer.camera;
	//获取视图中心点	
	var result = viewer.camera.pickEllipsoid(new Cesium.Cartesian2 ( viewer.canvas.clientWidth /2 , viewer.canvas.clientHeight / 2));
	var curPosition = Cesium.Ellipsoid.WGS84.cartesianToCartographic(result);
	var lon = curPosition.longitude*180/Math.PI;
	var lat = curPosition.latitude*180/Math.PI;
	var height=scene.globe.ellipsoid.cartesianToCartographic(viewer.camera.position).height;//curPosition.height;
	var cameraX = scene.camera.direction.x;
	var cameraY = scene.camera.direction.y;
	var cameraZ = scene.camera.direction.z;
	$("#extent",self.parent.document).text(lon+","+lat+","+height+","+"A,"+cameraX+","+cameraY+","+cameraZ);
	
   mouseAtTheMap=true;
});
var mapHandler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
var mouseAtTheMap=false;
handler.setInputAction(function(click) {
   // 处理鼠标抬起事件
   // 获取鼠标当前位置
   mouseAtTheMap=true;
 }, Cesium.ScreenSpaceEventType.LEFT_UP);

	function computeCircle(radius) {
    var positions = [];  
    for (var i = 0; i < 6; i++) { 
		var j = i*60;
        var radians = Cesium.Math.toRadians(j);  
        positions.push(new Cesium.Cartesian2(  
            radius * Math.cos(radians), radius * Math.sin(radians)));  
    }  
    return positions;  
	}  
	   
	function showShapeBall(viewer,ps,pointx,pointy,diameter)
	{
		var col;

		if(ps>=0)
		{
			col = Cesium.Color.LIGHTSLATEGREY;
		}
		
		if(ps>=0)
		{
			viewer.entities.add({ 
			name : 'show ball',
			position: Cesium.Cartesian3.fromDegrees(pointx, pointy,diameter), 
			ellipsoid : {  
			
			radii : new Cesium.Cartesian3(diameter, diameter, diameter),  
			material : col,   
			}
			});
		} 
	
		viewer.zoomTo(viewer.entities);
	}
	
	function showVec(){
		viewer.show = false;
		labelBall.show = true;
	}
	function showImg(){
		viewer.show = true;
		labelBall.show = false;
	}

	function showLabel(viewer,pointx,pointy,diameter,labelText,object){
		viewer.entities.add({
			name : 'label',
			id:labelText,
			position : Cesium.Cartesian3.fromDegrees(pointx, pointy,diameter),
			label : {
			  text : labelText,
			  font : '34pt monospace',
			  style: Cesium.LabelStyle.FILL_AND_OUTLINE,
			  outlineWidth : 2,
			  //水平位置
			  horizontalOrigin:Cesium.HorizontalOrigin.RIGHT,
			  //垂直位置
			  verticalOrigin : Cesium.VerticalOrigin.TOP,
			  //中心位置
			  pixelOffset : object
			}
		  });
		  viewer.zoomTo(viewer.entities);
	}
	
	// // if(label == 'A'){
	Cesium.loadJson('../../Test/BRTsTOPS.json').then(function(jsonData) {


		var high = 150;
		showLabel(viewer,153.017039,-27.474071,high,"1",new Cesium.Cartesian2(60, -35));
		showLabel(viewer,153.015945,-27.496553,10,"2",new Cesium.Cartesian2(-2, -10));
		showLabel(viewer,153.064253,-27.54355,high,"3",new Cesium.Cartesian2(35, -8));

		var p=0;
		var MAX_SUM_CO = 150;
		for(var i=0;i<jsonData.length;i++)
	
		{
			//debugger;
			var _tmpPush=[];
			var pointx = 0.0;
			var pointy = 0.0;
			var diameter = 150.0;
			if("Point" == jsonData[i].json_geometry.type)
			{
					_tmpPush=[];

					pointx=jsonData[i].json_geometry.coordinates[0];
					pointy=jsonData[i].json_geometry.coordinates[1];
					showShapeBall(viewer,MAX_SUM_CO,pointx,pointy,diameter);
			} 
			//debugger;
			
		}

		
	 })




// }else 


$("#pos",self.parent.document).click(function(){
	viewer.camera.flyTo({  
		destination : Cesium.Cartesian3.fromDegrees(153.02283,-27.50,28000.0) ,
		orientation: {
            heading : Cesium.Math.toRadians(0.0), // 方向
            pitch : Cesium.Math.toRadians(-89.0),// 倾斜角度
            roll : 0
        },
		complete: function () {
            // 到达位置后执行的回调函数
			viewer.entities.getById("1").show = true; 
			viewer.entities.getById("2").show = true; 
			viewer.entities.getById("3").show = true; 
        }	
	});
	
	});


$("#earth",self.parent.document).click(function(){
	viewer.camera.flyTo({  
		destination : Cesium.Cartesian3.fromDegrees(153.02283,-27.45642,20000000.0)  
	});
	viewer.entities.getById("1").show = false; 
	viewer.entities.getById("2").show = false; 
	viewer.entities.getById("3").show = false; 
	});
	

// $("#extent",self.parent.document).bind('DOMNodeInserted', function (e) {
	// var latlng = $("#extent",self.parent.document).text();debugger;
   // if(latlng.length > 3&&!mouseAtTheMap){
	  // var latlngArr = latlng.split(',');
	  // if(latlngArr[3]!=null){
	  // viewer.camera.flyTo({  
		  // destination : Cesium.Cartesian3.fromDegrees(parseFloat(latlngArr[0]),parseFloat(latlngArr[1]),parseFloat(latlngArr[2]))
	  // }); 
	  // }
   // }
   // mouseAtTheMap=false;
// });
	$("#layerVec",self.parent.document).click(function(){
		showVec();
	});
	$("#layerImg",self.parent.document).click(function(){
		showImg();
	});
	
	$('.cesium-viewer-bottom').remove();
	$('.cesium-viewer-toolbar').remove();
	$('.cesium-viewer-animationContainer').remove();
	$('.cesium-viewer-timelineContainer').remove();
}, undefined, true);
